const gulp          = require('gulp');
//const middleware    = require('gulp-middleware');
const browserSync   = require('browser-sync');
const spa           = require('browser-sync-spa');

const proxyMiddleware = require('http-proxy-middleware');
const browserSyncConf = require('../conf/browsersync.conf');
const browserSyncDistConf = require('../conf/browsersync-dist.conf');

browserSync.use(spa());

gulp.task('browsersync', browserSyncServe);
gulp.task('browsersync:dist', browserSyncDist);

function browserSyncServe(done) {
  browserSync.init(browserSyncConf());
  done();
}

function browserSyncDist(done) {
  browserSync.init(browserSyncDistConf());
  done();
}
