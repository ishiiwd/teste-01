angular
    .module('app')
    .component('app', {
        templateUrl: 'app/netflix.html',
        controller: ['$scope', '$http', function($scope, $http) {

            var $ctrl = this;
            var tipo = ['', 'title', 'director', 'actor']
            $scope.title    = 'Busca de Filmes';
            $scope.filmes   = '';

            $ctrl.$onInit = function() {
                $ctrl.$buscarFilmeSerie();
            };

            $ctrl.$buscarFilmeSerie = function() {

                if ($ctrl.tipo && $ctrl.busca)
                {
                    $scope.filmes = '';
                    $http.get('http://netflixroulette.net/api/api.php?'+tipo[$ctrl.tipo]+'='+$ctrl.busca).then(function(response) {
                        $scope.filmes = angular.isArray(response.data) ? response.data : [response.data];
                        console.log(response.data);
                    }, function(err) {
                        console.log(err);
                    });
                }

            };

        }]
    });