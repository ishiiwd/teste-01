# TESTE - BUSCA DE FILMES NETFLIX

Desenvolvido em AngularJS com testes em Karma.

## Observações

  O projeto foi inicializado utilizando FountainJS Generator Angular 1.

* [generator-fountain-angular1](https://github.com/FountainJS/generator-fountain-angular1) - Link com instruções para o gerador.


## Instalação e Dependências

É necessário NodeJS, npm e bower, as dependências podem ser instaladas com os comandos abaixo:


```sh
$ npm install
$ bower install
```

Depois de tudo instalado, pode rodar o servidor com o comando: 

```sh
$ gulp serve
```

A aplicação iniciará em http://localhost:3000

Caso não consiga visualizar a busca devido o problema de bloqueio do CORS (Access-Control-Allow-Origin), o Google Chrome possui um plugin para simular as condições necessárias de navegação.

* [Allow-Control-Allow-Origin: *](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi)

## netflix.js

O core do projeto com as definições de busca de filmes estão dentro do componente AngularJS.

```sh
src/app/netflix.js
```

## Api Netflix Roulette

A Api Restfull http://netflixroulette.net/api/ na versão Json possui muitas limitações. As opções de filtro é apenas por Título, Diretor ou Ator/Atriz.


## Licença

Projeto exclusivamente desenvolvido para avaliação da Sennit Tecnologia.